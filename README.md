erts
====================================================

Electronic Repair Ticket System (erts) is just a project I'm working on in my spare time. I built it primarily to keep track of all of the electronics that my kids broke, but I've also inherited some broken electronics that I work on as well.

It uses CodeIgniter framework and HMVC plugin to support multiple modules. Ialso added a QR code library which allows you to auto-generate a QR code foreach item you add to the database. This allows you to print out a label with aQR code on it to easily scan and pull up the item.

Notice: 
----------------------------------------------------

This project is meant to be run on a home server without external access. I did not take any sort of security measures to make sure that any data store will not be leaked or hacked.

This software is offered without warranty. If your data is lost, CPU explodes, or accidentally divide by zero and create a singularity, that is your problem. Please read the license!

If you want to support me, check out my website for information on how to donate. www.decompile-everything.com

