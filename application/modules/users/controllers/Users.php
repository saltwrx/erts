<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MX_Controller {

function __construct() {
	parent::__construct();
}

function index_get() {
	$this->load->model('store_accounts/mdl_store_accounts');

	$id = $this->mdl_store_accounts->get('id');

	if ($id == '') {
		$product = $this->mdl_store_accounts->get
	}
}

function _logged_in ($username) {
	//Session variable and redirect to admin panel.
	$query = $this->get_where_custom('username', $username);
	foreach($query->result() as $row) {
		$user_id = $row->id;
	}

	$this->session->set_userdata('user_id', $user_id);

	redirect('dashboard/home');
}

function submit() {
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username','Username','required|max_length[30]|xss_clean');
		$this->form_validation->set_rules('pass','Password','required|max_ength[30]|xss_clean|callback_password_check');

		if ($this->form_validation->run($this) == FALSE) {
			$this->login();
		}
		else {
			$username = $this->input->post('username', TRUE);
			$this->_logged_in($username);
		}
}

function password_check($pass, $query) {

	$username = $this->input->post('username', TRUE);
	$password = Modules::run('site_security/create_hash', $pass);

	$this->load->model('mdl_users');
	$dbHash = $this->mdl_users->get_dbhash($query);

	if (crypt($pass, $dbHash) == $dbHash) {

			$this->load->model('mdl_users');
			$result = $this->mdl_users->password_check($username, $pass);

			if ($result == FALSE) {
				$this->form_validation->set_message('password_check', 'Username or Password was incorrect.');
				return FALSE;
			}
			else {
				return TRUE;
			}
	} else {
		$this->form_validation->set_message('password_check', 'Username or Password was incorrect.');
		return FALSE;
	}
}

function login() {
	$data['view_file'] = "loginform";
	$this->load->module('templates');
	$this->templates->txrr_template($data);
}

function get($order_by) {
$this->load->model('mdl_users');
$query = $this->mdl_users->get($order_by);
return $query;
}

function get_with_limit($limit, $offset, $order_by) {
$this->load->model('mdl_users');
$query = $this->mdl_users->get_with_limit($limit, $offset, $order_by);
return $query;
}

function get_where($id) {
$this->load->model('mdl_users');
$query = $this->mdl_users->get_where($id);
return $query;
}

function get_where_custom($col, $value) {
$this->load->model('mdl_users');
$query = $this->mdl_users->get_where_custom($col, $value);
return $query;
}

function _insert($data) {
$this->load->model('mdl_users');
$this->mdl_users->_insert($data);
}

function _update($id, $data) {
$this->load->model('mdl_users');
$this->mdl_users->_update($id, $data);
}

function _delete($id) {
$this->load->model('mdl_users');
$this->mdl_users->_delete($id);
}

function count_where($column, $value) {
$this->load->model('mdl_users');
$count = $this->mdl_users->count_where($column, $value);
return $count;
}

function get_max() {
$this->load->model('mdl_users');
$max_id = $this->mdl_users->get_max();
return $max_id;
}

function _custom_query($mysql_query) {
$this->load->model('mdl_users');
$query = $this->mdl_users->_custom_query($mysql_query);
return $query;
}

}
