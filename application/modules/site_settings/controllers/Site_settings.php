<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Site_settings extends MX_Controller
{

function __construct() {
parent::__construct();
}

function _get_mapCode()
{
	$mapCode = '';
	return $mapCode;
}

function _get_address()
{
	$address = '';
	$address.= '';
	return $address;
}

function _get_telnum()
{
	$telnum = '';
	return $telnum;
}

function _get_paypal_email()
{
	$email = '';
	return $email;
}

function _get_compName()
{
	$compName = '';
	return $compName;
}

function _get_ownerEmail()
{
	$ownerEmail = 'test@email.com';
	return $ownerEmail;
}

function _get_support_team_name()
{
	$name = "Technical Support";
	return $name;
}

function _get_welcome_message($customer_id)
{
	$msg = "Hello!";
	return $msg;
}

function _get_cookie_name()
{
	$cookie_name = 'random';
	return $cookie_name;
}

function _get_currency_symbol()
{
	$symbol = "&#36;";
	return $symbol;
}

function _get_currency_code()
{
	$code = 'USD';
	return $code;
}

function _get_recaptcha_key()
{
	// Returns the Google Recaptcha Private Key for validation
	$key = '';
	return $key;
}

function get_max_category_depth()
{
	$max_depth = 2;
	return $max_depth;
}

function get_breadcrumbs()
{
	$first_bit = base_url();
	$second_bit = $this->uri->segment(1);
	$third_bit = $this->uri->segment(2);
	$fourth_bit = $this->uri->segment(3);

	if ($second_bit == '') {
		$crumbs = '<a class="crumb" href='.$first_bit.'>Home</a>';
	} elseif ($third_bit == '') {
		$crumbs = '<a class="crumb" href='.$first_bit.'>Home</a> / <a class="crumb" href='.$first_bit.$second_bit.'>'.$second_bit.'</a>';
	} elseif ($fourth_bit == '') {
		$crumbs = '<a class="crumb" href='.$first_bit.'>Home</a> / <a class="crumb" href='.$first_bit.$second_bit.'>'.$second_bit.'</a> / <a class="crumb" href='.$first_bit.$second_bit.'/'.$third_bit.'>'.$third_bit.'</a>';
	} else {
		$crumbs = '<a class="crumb" href='.$first_bit.'>Home</a> / <a class="crumb" href='.$first_bit.$second_bit.'>'.$second_bit.'</a> / <a class="crumb" href='.$first_bit.$second_bit.'/'.$third_bit.'>'.$third_bit.'</a> / <a class="crumb" href='.$first_bit.$second_bit.'/'.$third_bit.'/'.$fourth_bit.'>'.$fourth_bit.'</a>';
	}

	return $crumbs;
}

function _get_page_not_found_msg()
{
	$msg="<h1>404 Not Found</h1>";
	$msg.="<p>The page you were looking for was not found. Please hit the back button or use the links below to navigate.</p><br /><br />";
	$msg.="- <a href='".base_url()."'>Home</a><br /><br />";
	$msg.="- <a href='".base_url()."/About'>About</a><br /><br />";
	$msg.="- <a href='".base_url()."/contactus'>Contact Us</a><br /><br />";
	return $msg;
}

}
