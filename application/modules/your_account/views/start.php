<?php
	$form_location = base_url().'your_account/submit';
?>
	<div class="blogCard"  style="margin-top:36px;margin-bottom:162px;">
	  <div class="bcardHead">
	    <div class="blogTitle"><a href="#"><i class="fas fa-check-square"></i>  &nbsp;&nbsp; Create A New Account</a></div>
	  </div>
	  <div class="bcardBody" style="padding-left: 36px;">
			<div>
				<h2>Please fill in your details and click 'YES' to create your user account.</h2>
				<br />
				<form class="form-horizontal col-md-12" action="<?= $form_location ?>" method="post">
				<fieldset>

				<div class="form-group">
				  <div class="col-md-6">
				  <input id="username" name="username" type="text" value="<?= $username ?>" placeholder="Username" class="form-control input-md" required="">

				  </div>
				</div>

				<!-- Text input-->
				<div class="form-group">
				  <div class="col-md-6">
				  <input id="textinput" name="email" type="text" value="<?= $email ?>" placeholder="E-mail" class="form-control input-md">

				  </div>
				</div>

				<!-- Text input-->
				<div class="form-group">
				  <div class="col-md-6">
				  <input id="textinput" name="pword" type="password" value="<?= $pword ?>" placeholder="Password" class="form-control input-md" required="">

				  </div>
				</div>

				<!-- Text input-->
				<div class="form-group">
				  <div class="col-md-6">
				  <input id="textinput" name="confirm_pword" type="password" value="<?= $confirm_pword ?>" placeholder="Confirm Password" class="form-control input-md" required="">

				  </div>
				</div>

				<div class="form-group">
				  <div class="col-md-6">
						<div class="g-recaptcha" data-sitekey="6Lc0T2sUAAAAAIPMO15Z82xG2ryfu_iAZhbptmPE"></div>
					</div>
				</div>

				<br />

				<!-- Button -->
				<div class="form-group">
				  <div class="col-md-6">
					<button id="singlebutton" name="submit" value="Submit" class="btn btn-md btn-primary">Register</button>
				  </div>
				</div>

				</fieldset>
				</form>
			</div>

			<?php
				echo validation_errors("<div class='alert alert-danger' role='alert' style='width: 87%;'>", "</div>");

				if (isset($flash))
				{
					echo $flash;
				}
			?>
		</div>
	</div>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<!--?= Modules::run('templates/side_cards') ?-->
