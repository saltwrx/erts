<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Your_account extends MX_Controller
{

function __construct() 
{
	parent::__construct();

	$this->load->library('form_validation');
	$this->form_validation->CI =& $this;
}

function log_out()
{
	unset($_SESSION['user_id']);
	$this->load->module('site_cookies');
	$this->site_cookies->_destroy_cookie();
	redirect(base_url());
}

function welcome()
{
	$this->load->module('site_security');
	$this->site_security->_check_is_logged_in();
	$data['flash'] = $this->session->flashdata('item');
	$data['view_file'] = "welcome";
	$this->load->module('templates');
	$this->templates->txrr_template($data);
}

function start()
{
	$data = $this->fetch_data_from_post();
	$data['flash'] = $this->session->flashdata('item');
	$data['view_file'] = "start";
	$this->load->module('templates');
	$this->templates->txrr_template($data);
}

function fetch_data_from_post()
{
	$data['username'] = $this->input->post('username', TRUE);
	$data['email'] = $this->input->post('email', TRUE);
	$data['pword'] = $this->input->post('pword', TRUE);
	$data['confirm_pword'] = $this->input->post('confirm_pword', TRUE);
	return $data;
}

function submit_login()
{
	$submit = $this->input->post('submit', TRUE);
	
	if ($submit=="Submit")
	{
		//process the form
		$this->load->library('form_validation');
		$this->form_validation->set_rules('pword', 'Password', 'required|min_length[4]|max_length[35]');
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[7]|max_length[60]|callback_username_check');
		
		if ($this->form_validation->run() == TRUE)
		{
			// Figure out the user ID
			$col1 = 'username';
			$value1 = $this->input->post('username', TRUE);
			$col2 = 'email';
			$value2 = $this->input->post('username', TRUE);
			$query = $this->store_accounts->get_with_double_condition($col1, $value1, $col2, $value2);
			foreach ($query->result() as $row)
			{
				$user_id = $row->id;
			}
			
			$remember = $this->input->post('remember', TRUE);
			if ($remember == "remember-me")
			{
				$login_type = "longterm";
			}
			else
			{
				$login_type = "shortterm";
			}
			
			$data['last_login'] = time();
			$this->store_accounts->_update($user_id, $data);
			
			// Send them to account page
			$this->_in_you_go($user_id, $login_type);
		}
		else
		{
			echo validation_errors();
		}
	}
}

function _in_you_go($user_id, $login_type)
{
	// Login type can be long-term (cookie) or short-term (session)
	if ($login_type == "longterm")
	{
		// Set cookie
		$this->load->module('site_cookies');
		$this->site_cookies->_set_cookie($user_id);
	}
	else
	{
		// Set session variable
		$this->session->set_userdata('user_id', $user_id);
	}
	
	// Check if cart has items and update shopper ID
	$this->_attempt_swap_cart($user_id);
}

function _attempt_swap_cart($user_id)
{
	// Checking add_to_basket table to see if session ID exists
	// If session ID exists, we update shopper ID for the session (customer account exists)
	$this->load->module('add_to_basket');
	$customer_session_id = $this->session->session_id;
	
	$col1 = 'session_id';
	$value1 = $customer_session_id;
	$col2 = 'shopper_id';
	$value2 = 0;
	
	$query = $this->add_to_basket->get_with_double_condition($col1, $value1, $col2, $value2);
	
	$num_rows = $query->num_rows();
	
	if ($num_rows > 0)
	{
		// Found record to update with shopper ID
		$mysql_query = "update add_to_basket set shopper_id=$user_id where session_id='$customer_session_id'";
		$query = $this->add_to_basket->_custom_query($mysql_query);
		redirect('cart');
	}
	else
	{
		// Send to user page
		redirect('your_account/welcome');
	}
}

function submit()
{
	$submit = $this->input->post('submit', TRUE);
	$username = $this->input->post('username', TRUE);
	
	if ($submit=="Submit")
	{
		// Process the form
		$this->load->library('form_validation');
		$this->form_validation->set_rules('pword', 'Password', 'required|min_length[4]|max_length[35]');
		$this->form_validation->set_rules('confirm_pword', 'Confirm Password', 'required|matches[pword]');
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[7]|max_length[60]');//|is_unique[store_accounts.username]');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[120]');
		
		if ($this->form_validation->run() == TRUE)
		{
			// Is username unique?
			$user_check = $this->_username_unique($username);
			
			if ($user_check == FALSE)
			{
				// If username unique, process the fuckin shit and redirect.
				$this->_process_create_account();
				
				$flash_msg = "Username successfully created. Please login from the menu.";
				$value = "<div class='alert alert-success' role='alert' style='width: 87%;'>".$flash_msg."</div>";
				$this->session->set_flashdata('item', $value);
				redirect('your_account/login');
			}
			else
			{
				// If username is not unique, yell at the customer.
				$flash_msg = "The username you have selected is not available.";
				$value = "<div class='alert alert-danger' role='alert' style='width: 87%;'>".$flash_msg."</div>";
				$this->session->set_flashdata('item', $value);
				$this->start();
			}
		}
		else
		{
			$this->start();
		}
	}
}

function _username_unique($username)
{
	// Load the store_accounts module first, dumbass
	$this->load->module('store_accounts');
	
	// Checking if username is unique manually since CI is_unique validation is being gay
	$query = $this->store_accounts->get_where_custom('username', $username);
	$num_rows = $query->num_rows();
	if ($num_rows < 1)
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

function _process_create_account()
{
	$this->load->module('store_accounts');
	$data = $this->fetch_data_from_post();
	unset($data['confirm_pword']);
	$data['date_made'] = time();
	$pword = $data['pword'];
	$this->load->module('site_security');
	$data['pword'] = $this->site_security->_hash_string($pword);
	$this->store_accounts->_insert($data);
}

function login()
{
	$data['username'] = $this->input->post('username', TRUE);
	$this->load->module('templates');
	$this->templates->login_page($data);
}

function username_check($str) 
{
	
	$this->load->module('store_accounts');
	$this->load->module('site_security');
	$error_msg = 'You did not enter a valid username or password.';
	
	$col1 = 'username';
	$value1 = $str;
	$col2 = 'email';
	$value2 = $str;
	$query = $this->store_accounts->get_with_double_condition($col1, $value1, $col2, $value2);
	
	$num_rows = $query->num_rows();
	
	if ($num_rows < 1)
	{
		$this->form_validation->set_message('username_check', $error_msg);
		return FALSE;
	}
	
	foreach ($query->result() as $row)
	{
		$pword_on_table = $row->pword;
	}
	
	$pword = $this->input->post('pword', TRUE);
	$result = $this->site_security->_verify_hash($pword, $pword_on_table);
	
	if ($result == TRUE)
	{
		return TRUE;
	}
	else
	{
		$this->form_validation->set_message('username_check', $error_msg);
		return FALSE;
	}
}

}
