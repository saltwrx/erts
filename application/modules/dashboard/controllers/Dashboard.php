<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends MX_Controller
{

function __construct() {
parent::__construct();
Modules::run('site_security/admin_check');
}

function home()
{
	$this->load->module('site_security');
	$this->site_security->_admin_check();
	$data['username'] = $this->session->userdata('username');
	$data['flash'] = $this->session->flashdata('item');
	$data['view_file'] = "home";
	$this->load->module('templates');
	$this->templates->cm_panel($data);
}

}
