<html>
	<head>
		<?php
			if (isset($draw_editor)) {
				include('editor_header.php');
			}

			$adminName = Modules::run('site_security/_get_user_id');
		?>

		<meta charset="utf-8" />
		<!-- include libraries(jQuery, bootstrap) -->
		<link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
		<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

		<!-- include summernote css/js -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

		<link rel="stylesheet" type="text/css" href="<?=base_url()?>/assets/css/cm_panel.css" />
		<title>Admin</title>
	</head>

	<body>
		<div class="wrapper">
			<div class="header">
				<div class="logo">
					<img src="<?=base_url()?>/assets/img/h3_molecule.png" alt="h3 molecule" /> &nbsp; <a href="#">HELIUM-3 <span>Admin Panel</span></a>
				</div>
			</div>

			<div class="mainContent">
				<div class="sideNav">
					<ul class="nav">
						<li><a href="<?= base_url().'dashboard/home' ?>"><span class='glyphicon glyphicon-dashboard' aria-hidden='true'></span> &nbsp;&nbsp; Dashboard</a></li>
						<li><a href="<?= base_url().'enquiries/inbox' ?>"><span class='glyphicon glyphicon-envelope' aria-hidden='true'></span> &nbsp;&nbsp; Inbox</a></li>
						<li><a href="<?= base_url().'webpages/manage' ?>"><span class='glyphicon glyphicon-duplicate' aria-hidden='true'></span> &nbsp;&nbsp; CMS</a></li>
						<li><a href="<?= base_url().'blog/manage' ?>"><span class='glyphicon glyphicon-align-justify' aria-hidden='true'></span> &nbsp;&nbsp; Manage Blog</a></li>
						<li><a href="<?= base_url().'blog_categories/manage' ?>"><span class='glyphicon glyphicon-list-alt' aria-hidden='true'></span> &nbsp;&nbsp; Manage Blog Categories</a></li>
						<li><a href="<?= base_url().'store_items/manage' ?>"><span class='glyphicon glyphicon-tags' aria-hidden='true'></span> &nbsp;&nbsp; Manage Items</a></li>
						<li><a href="<?= base_url().'store_categories/manage' ?>"><span class='glyphicon glyphicon-list-alt' aria-hidden='true'></span> &nbsp;&nbsp; Manage Item Categories</a></li>
						<li><a href="<?= base_url().'store_order_status/view_orders' ?>"><span class='glyphicon glyphicon-book' aria-hidden='true'></span> &nbsp;&nbsp; View Orders</a></li>
						<li><a href="<?= base_url().'store_order_status/manage' ?>"><span class='glyphicon glyphicon-book' aria-hidden='true'></span> &nbsp;&nbsp; Order Status Options</a></li>
						<li><a href="<?= base_url().'homepage_blocks/manage' ?>"><span class='glyphicon glyphicon-star' aria-hidden='true'></span> &nbsp;&nbsp; Homepage Offers</a></li>
						<li><a href="<?= base_url().'store_accounts/manage' ?>"><span class='glyphicon glyphicon-user' aria-hidden='true'></span> &nbsp;&nbsp; Manage Accounts</a></li>
						<li><a href="#"><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> &nbsp;&nbsp; Link</a></li>
						<li><a href="<?= base_url().'h3_portal/logout' ?>"><span class='glyphicon glyphicon-log-out' aria-hidden='true'></span> &nbsp;&nbsp; Log Out</a></li>
					</ul>
				</div>

				<div class="container" class="clearfix">
					<div class="subContent">
						<?php
							if (!isset($view_file))
							{
								$view_file = $this->uri->segment(2);
							}

							if (!isset($module))
							{
								$module = $this->uri->segment(1);
							}

							if ($module == 'blog')
							{
								$editor = '#blog_content';
							}
							elseif ($module == 'webpages')
							{
								$editor = '#page_content';
							}

							if (($view_file!="") && ($module!=""))
							{
								$path = $module."/".$view_file;
								$this->load->view($path);
							}
						?>
					</div>
				</div>
			</div>

			<div class="footer">
				<div class="designer">
					<a href="#">HELIUM-3 <span>Admin Panel &copy; 2015 - Designed by Adan Salinas</span></a>
				</div>
			</div>
		</div>
	</body>
	<script>
		$(document).ready(function() {
			$('<?= $editor ?>').summernote();
		});
	</script>
</html>
