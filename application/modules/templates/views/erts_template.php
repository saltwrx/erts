<!DOCTYPE html>
<html>
	<head>
		<title>Texas Road Racers - From Amateur to Pro</title>
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Nothing+You+Could+Do&display=swap" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/texasrr_template.css">
	</head>
	<body>

		<!-- Facebook Stuff-->

		<div id="fb-root"></div>
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3"></script>
		
		<!-- Navbar -->

    <div class="col-xl-12 navigation">
      <div class="container">
        <div class="row">
          <div class="col-xl-12 nav">
						<?= Modules::run('webpages/navigation') ?>
          </div>
        </div>
      </div>
    </div>

		<!-- Main -->
		<div class="col-xl-12 header-img-display">
			<div class="col-xl-3 header-cover">
				Don't<br>
				Spin<br>
				Out!
			</div>
		</div>
		<div class="container main">
			<div class="col-xl-12">
				<div class="row">
					<?php

		        if ($customer_id > 0)
		        {
		          //include_once('customer_panel_top.php');
		        }

		        if (isset($headline))
		        {
							$left_box_style = '<div class="col-md-8 left-box">';

		          if ($headline == "home")
		          {
								$left_box_style = '';
		            $headline = "";
		            $page_content = "";
		          }

		          echo $left_box_style.'<h1>'.$headline.'</h1>';
		        }

		        if (!isset($view_file))
		        {
		          $view_file = "";
		        }

		        if (!isset($view_module))
		        {
		          $view_module = $this->uri->segment(1);
		        }

		        if(isset($page_content))
		        {
		          if (!isset($headline))
		          {
		            $headline = '';
		          }

		          if ($headline != "home" && $headline != '')
		          {
		            echo $page_content.'</div>';
								require_once('side_cards.php');
		          }

		          if (!isset($page_url))
		          {
		            $page_url = 'home';
		          }
		          if ($page_url == 'home')
		          {
		            require_once('homepage_content.php');
		          }
		        }
		        elseif (($view_file!="") && ($view_module!=""))
		        {
		          $path = $view_module."/".$view_file;
		          $this->load->view($path);
		        }
		      ?>
				</div>
			</div>
		</div>

    <!-- Footer -->

    <div class="col-xl-12 footer">
      <div class="container">
        <div class="col-xl-12" style="text-align: center;">
					<div class="row">
						<div class="col-xl-4 left-box2">

						</div>
						<div class="col-xl-4 center-box">
							Texas Road Racers &copy; <span id="year">2018</span> <br> Designed by <strong><a href="https://www.saltwrx.org">SALTWRX</a></strong>
						</div>
						<div class="col-xl-4 right-box2">

						</div>
					</div>
        </div>
      </div>
    </div>

		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</body>
	<script>
    document.getElementById("year").innerHTML = new Date().getFullYear();
	</script>
</html>
