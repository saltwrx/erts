<header>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <!-- Slide One - Set the background image for this slide in the line below -->
      <div class="carousel-item active" style="background-image: url('<?= base_url() ?>assets/img/saltwrx_carousel3.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <h3>IT Consultations</h3>
          <p>Helping small businesses and start-ups grow.</p>
        </div>
      </div>
      <!-- Slide Two - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('<?= base_url() ?>assets/img/saltwrx_carousel2.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <h3>Mobile Web Applications</h3>
          <p>Expand your applications to all platforms.</p>
        </div>
      </div>
      <!-- Slide Three - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('<?= base_url() ?>assets/img/saltwrx_carousel4.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <h3>Custom Applications</h3>
          <p>Built to suit your company's needs.</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</header>

<!-- Page Content -->
<div class="container">

  <h1 class="my-4">IT & Software Services</h1>

  <!-- Marketing Icons Section -->
  <div class="row">
    <div class="col-lg-4 mb-4">
      <div class="card h-100">
        <h4 class="card-header">IT Consultations</h4>
        <div class="card-body">
          <p class="card-text">With network infrastructure and security in mind, we can help your company plan out and build your network according to your needs -- Even on a budget! From hosted or on-premise PBX phone systems to secure network appliances, we've got your back!</p>
        </div>
        <div class="card-footer">
          <a href="<?= base_url() ?>saltwrx/contact" class="btn btn-primary">Learn More</a>
        </div>
      </div>
    </div>
    <div class="col-lg-4 mb-4">
      <div class="card h-100">
        <h4 class="card-header">Mobile Applications</h4>
        <div class="card-body">
          <p class="card-text">Need to take your project to the next level? Need to expand to the latest handheld devices to keep up with the times? Let use design and implement your mobile dream application for you!</p>
        </div>
        <div class="card-footer">
          <a href="<?= base_url() ?>saltwrx/contact" class="btn btn-primary">Learn More</a>
        </div>
      </div>
    </div>
    <div class="col-lg-4 mb-4">
      <div class="card h-100">
        <h4 class="card-header">Custom Web Applications</h4>
        <div class="card-body">
          <p class="card-text">Tired of looking at the same old static interface that doesn't seem to adapt to your modern hardware or device? We can fix that! From custom REST APIs to beautifully designed, dynamic interfaces -- We've got you covered!</p>
        </div>
        <div class="card-footer">
          <a href="<?= base_url() ?>saltwrx/contact" class="btn btn-primary">Learn More</a>
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->

  <!-- Portfolio Section -->
  <h2>Our Projects</h2>

  <div class="row">
    <div class="col-lg-4 col-sm-6 portfolio-item">
      <div class="card h-100">
        <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
        <div class="card-body">
          <h4 class="card-title">
            <a href="https://www.rgvrocks.com">RGVRocks.Com</a>
          </h4>
          <p class="card-text">Helping people in the RGV discover local music and talent, RGVRocks offers venue promotion and a unique social media experience. Check it out at <a href="https://www.rgvrocks.com">RGVRocks.Com</a>!</p>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-sm-6 portfolio-item">
      <div class="card h-100">
        <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
        <div class="card-body">
          <h4 class="card-title">
            <a href="https://www.imgdump.site">IMGDump.Site</a>
          </h4>
          <p class="card-text">Upload images anonymously or create your own album to share your images with friends! Create your album today at <a href="https://www.imgdump.site">IMGDump.Site</a>!</p>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-sm-6 portfolio-item">
      <div class="card h-100">
        <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
        <div class="card-body">
          <h4 class="card-title">
            <a href="https://www.afslabs.net">AFSLabs</a>
          </h4>
          <p class="card-text">AFSLabs is a place to learn about latest technology and how to make it work for you! If you enjoy hacking and tinkering, <a href="https://www.afslabs.net">AFSLabs</a> is the place for you!</p>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-sm-6 portfolio-item">
      <div class="card h-100">
        <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
        <div class="card-body">
          <h4 class="card-title">
            <a href="#">Coming Soon!</a>
          </h4>
          <p class="card-text">We know what you want!</p>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-sm-6 portfolio-item">
      <div class="card h-100">
        <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
        <div class="card-body">
          <h4 class="card-title">
            <a href="#">Coming Soon!</a>
          </h4>
          <p class="card-text">We got what you need!</p>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-sm-6 portfolio-item">
      <div class="card h-100">
        <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
        <div class="card-body">
          <h4 class="card-title">
            <a href="#">Coming Soon!</a>
          </h4>
          <p class="card-text">Just a matter of time!</p>
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->

  <!-- Features Section -->
  <div class="row">
    <div class="col-lg-6">
      <h2>Modern Business Features</h2>
      <p>The Modern Business template by Start Bootstrap includes:</p>
      <ul>
        <li>
          <strong>Bootstrap v4</strong>
        </li>
        <li>jQuery</li>
        <li>Font Awesome</li>
        <li>Working contact form with validation</li>
        <li>Unstyled page elements for easy customization</li>
      </ul>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.</p>
    </div>
    <div class="col-lg-6">
      <img class="img-fluid rounded" src="http://placehold.it/700x450" alt="">
    </div>
  </div>
  <!-- /.row -->

  <hr>

  <!-- Call to Action Section -->
  <div class="row mb-4">
    <div class="col-md-8">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
    </div>
    <div class="col-md-4">
      <a class="btn btn-lg btn-secondary btn-block" href="#">Call to Action</a>
    </div>
  </div>

</div>
<!-- /.container -->
