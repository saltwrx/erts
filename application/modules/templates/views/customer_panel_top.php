<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="<?= base_url() ?>your_account/welcome">Your Messages</a></li>
  <li role="presentation"><a href="#">Your Orders</a></li>
  <li role="presentation"><a href="#">Account Info</a></li>
  <li role="presentation"><a href="<?= base_url() ?>your_account/log_out">Sign Out</a></li>
</ul>