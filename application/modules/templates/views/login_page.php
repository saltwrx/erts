<?php
	$first_bit = $this->uri->segment(1);
	$form_location = base_url().$first_bit.'/submit_login';
?>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Gruppo|Iceland|Nova+Square|Press+Start+2P|Special+Elite" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/txrr_template.css">
		<script src='https://www.google.com/recaptcha/api.js'></script>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-lg-10">
					<div class="Form" align="center">

							<?php
								echo validation_errors("<div class='alert alert-danger' role='alert'>", "</div><br /><br />");

								if (isset($flash))
								{
									echo $flash;
								}
							?>

						<div class="formBox" align="center">

							<h1>Admin Portal</h1>
							<h4>Login</h4>
							<br />

							<?php

							echo form_open($form_location);


							echo form_input('username', '', 'class="form-control form-control-lg" placeholder="Username"');

							echo "<div class='formSpacer'>&nbsp;</div>";

							echo form_password('pword', '', 'class="form-control form-control-lg" placeholder="Password"');

							echo "<div class='formSpacer'>&nbsp;</div>";

						?>
						<div class="">
							<div class="container">
								<div class="row">
									<?php
											if ($first_bit == "your_account")
											{
												echo "<div class='col-sm-3' style='text-align: right;'><label>Remember Me</label></div><div class='col-sm-1' style='margin-top:8px;text-align: left;'>";
									?>
												<input type="checkbox" name="remember" value="remember-me">
									<?php
												echo "</div>";
											}
											else
											{
												echo "<br />";
											}
									?>
								</div>
							</div>

							<br />

							<?php
								echo '<div class="col-sm-12"><div class="container"><div class="row" style="text-align: left;"><div class="col-sm-12"><div class="g-recaptcha" data-sitekey="6Ld2p6oUAAAAAEItBM_TT-n0jLtUkfmf2Sc66B5o"></div>';

								echo '<div class="col-sm-12">&nbsp;</div>';

								echo '<div class="inputSubmit col-sm-12"><button type="submit" name="submit" value="Submit" class="btn btn-primary">Submit</button></div></div></div></div></div>';

								echo form_close();

								echo "<div class='formSpacer'>&nbsp;</div>";
								echo "<div class='formSpacer'>&nbsp;</div>";

							?>
							<div class="formSpacer">
								<a href="<?= base_url() ?>your_account/recover">Forgot Username?</a> | <a href="<?= base_url() ?>your_account/reset">Forgot Password?</a>
							</div>
						</div>

						</section>
					</div>
				</div>
			</div>
	</body>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
</html>
