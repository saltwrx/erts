<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Templates extends MX_Controller
{

	function _draw_breadcrumbs($data)
	{
		//$data needs template, current_page_title, and breadcrumbs_array
		$this->load->view('breadcrumbs_thundaga', $data);
	}

	function txrr_template($data)
	{
		if (!isset($data['view_module']))
		{
			$data['view_module'] = $this->uri->segment(1);
		}

		$this->load->module('site_security');
		$data['customer_id'] = $this->site_security->_get_user_id();

		$this->load->view('txrr_template', $data);
	}

	function cm_panel($data)
	{
		$this->load->view('cm_panel', $data);
	}

	function side_cards()
	{
		$this->load->view('side_cards');
	}

	function login_page($data)
	{
		$this->load->view('login_page', $data);
	}

	function register($data)
	{
		$this->load->view('register', $data);
	}

	function pass_reset($data)
	{
		$this->load->view('pass_reset', $data);
	}

	function user_recover($data)
	{
		$this->load->view('user_recover', $data);
	}

}
