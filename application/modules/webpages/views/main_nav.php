<ul class="navbar-nav mr-auto">
  <div class="row">
    <li>
      <a class="navi-link" href="<?= base_url() ?>"><img class="img-resp" src="<?= base_url() ?>assets/img/trr_logo.png" alt="texas road racers logo"> Texas Road Racers</a>
    </li>

    <?php

      $is_active = 'active';

      foreach($nav_query->result() as $row)
      {
        if ($active == $row->page_title) {
    ?>

          <li class="nav-item">
            <a class="navi-link <?= $is_active ?>" href="<?= base_url().$row->page_url ?>"><?= ucfirst($row->page_title) ?></a>
          </li>

    <?php
        } else {
    ?>

          <li class="nav-item">
            <a class="navi-link" href="<?= base_url().$row->page_url ?>"><?= ucfirst($row->page_title) ?></a>
          </li>

    <?php
        }
      }

      if ($active == 'blog') {
    ?>

    <li class="nav-item">
      <a class="navi-link active" href="<?= base_url() ?>blog/all">Blog</a>
    </li>

    <?php
      } else {

    ?>

    <li class="nav-item">
      <a class="navi-link" href="<?= base_url() ?>blog/all">Blog</a>
    </li>

    <?php

      }
    ?>


  </div>
</ul>

<!--
<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Stuff <span class="caret"></span></a>
  <ul class="dropdown-menu">
	< ?php
		foreach($nav_query->result() as $row)
		{
			echo '<li><a href="'.base_url().$row->page_url.'">'.$row->page_title.'</a></li>';
		}
	?>
  </ul>
</li>
-->
