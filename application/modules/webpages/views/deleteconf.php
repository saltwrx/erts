<h1><?= $headline ?></h1>

<?php
	if (isset($flash)) 
	{
		echo $flash;
	} 
?>
<div id="group_box">
		<div id="box_title">
			<span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> :-: &nbsp;&nbsp; Confirm Delete
		</div>
		<div id="box_options">

			<p>Are you sure you want to delete this webpage?</p>
			
			<?php echo form_open('webpages/delete/'.$update_id); ?>
			
			<input class="btn btn-danger" type="submit" name="submit" value="Yes - Delete Webpage" /> <input class="btn btn-default" type="submit" name="submit" value="Cancel" />
			
			</form>
		</div>
</div>