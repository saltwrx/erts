<?php
	$form_location = base_url().'webpages/create/'.$update_id;
?>

<h1>Create A New Webpage</h1>

<?php
if ($update_id > 2) 
{ ?>
<div id="group_box">
		<div id="box_title">
			<span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> :-: &nbsp;&nbsp; Webpage Options
		</div>
		<div id="box_options">
			<a type="button" class="btn btn-danger" href="<?= base_url() ?>webpages/deleteconf/<?= $update_id ?>">Delete Webpage</a>
		</div>
</div>
<?php
}
?>

<?php
	if (isset($flash)) 
	{
		echo $flash;
	} 
?>
	
	<?php
		echo validation_errors("<p style='color:red;'>","</p>");
		echo form_open($form_location);
	?>
	
<div id="group_box">
	<?php echo form_open($form_location); ?>
		<div id="box_title">
			<span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> :-: &nbsp;&nbsp; Webpage Details
		</div>
		<div id="box_inputs">
			<table id="form_table">
			<tr>
				<td colspan="3"><?= validation_errors("<p style='color: red;'>", "</p>") ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td id="form_label"><label for="textbox" class="control-label">Page Title</label></td>
				<td id="form_field">
					<?php
						$data = array(
									'class'              => 'form-control',
									'name'             =>  'page_title',
									'id'                  =>  'page_title',
									'value'             =>  $page_title,
									'maxlength'      =>  '230',
									'size'               =>  '50',
									'style'              =>  'width:400px'
									);
									
						echo form_input($data);
					?>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td id="form_label"><label for="textbox" class="control-label">Page Keywords</label></td>
				<td id="form_field">
					<?php
						$data = array(
									'class'              => 'form-control',
									'name'             =>  'keywords',
									'id'                  =>  'keywords',
									'value'             =>  $keywords,
									'rows'              =>  '5',
									'cols'               =>  '20',
									);
									
						echo form_textarea($data);
					?>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td id="form_label"><label for="textbox" class="control-label">Page Description</label></td>
				<td id="form_field">
					<?php
						$data = array(
									'class'              => 'form-control',
									'name'             =>  'description',
									'id'                  =>  'description',
									'value'             =>  $description,
									'rows'              =>  '5',
									'cols'               =>  '20',
									);
									
						echo form_textarea($data);
					?>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td id="form_label"><label for="textbox" class="control-label">Page Content</label></td>
				<td id="form_field">
					<?php
						$data = array(
									'class'              => 'form-control',
									'name'             =>  'page_content',
									'id'                  =>  'page_content',
									'value'             =>  $page_content,
									'rows'              =>  '15',
									'cols'               =>  '40',
									);
									
						echo form_textarea($data);
					?>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>	
	</div>
	<div id="box_actions">
		<button type="submit" name="submit" value="Submit" class="btn btn-primary">Save Changes</button> &nbsp;&nbsp; <button type="submit" name="submit" value="Cancel" class="btn btn-danger">Cancel</button>
	</div>
	<?php	echo form_close(); ?>	
</div>