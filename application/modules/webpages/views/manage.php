<h1>Content Management System</h1>

<?php
	$create_webpage_url = base_url()."webpages/create";
?>

<p style="margin-top: 38px;"><a href="<?= $create_webpage_url ?>"><button type="button" class="btn btn-primary">Create New Webpage</button></a></p>

<div id="group_box">
	<div id="box_title">
		<span class='glyphicon glyphicon-duplicate' aria-hidden='true'></span> :-: &nbsp;&nbsp; Webpages
	</div>
		<?php
			if (isset($flash)) 
			{
				echo $flash;
			} 
		?>
	<div id="box_inputs">
		<table id="form_table" class="table table-striped table-bordered">
			<tr>
				<th>Page URL</th>
				<th>Page Title</th>
				<th>Actions</th>
			</tr>
			<?php
				foreach($query->result() as $row) {
					
					$edit_url = base_url()."webpages/create/".$row->id;
					$delete_url = base_url()."webpages/delete/".$row->id;
					$view_url = base_url().$row->page_url;
					$page_title = $row->page_title;
					$page_url = $row->page_url;
					
			?>
			<tr>
				<td>
					<?php 
						echo $view_url;
					?>
				</td>
				<td>
					<?php 
						echo $page_title; 
					?>
				</td>
				<td>
					
					<a type='button' class='btn btn-primary' href='<?= $view_url ?>'><span class='glyphicon glyphicon-eye-open' aria-hidden='true'></span></a>
					<?php 
						$page_url = strtolower($row->page_url);
						
						if ($page_url == "store")  //In the case that you do not want to make home or other pages editeable
						{
							//Cannot modify these pages
							echo "<a type='button' class='btn btn-default' href='#'><span class='glyphicon glyphicon-minus' aria-hidden='true'></span></a>";
						} 
						else 
						{
							echo "<a type='button' class='btn btn-success' href=".$edit_url."><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a>";
						}
					?>
				</td>
			</tr>
			<?php
				}
			?>
		</table>
	</div>

</div>