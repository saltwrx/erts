<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Site_security extends MX_Controller
{

function __construct() {
	parent::__construct();
}

function _is_already_admin($update_id)
{
	$this->load->module('acp_sct');
	$is_already_admin = $this->acp_sct->get_where('user_id', $update_id);

	if ($is_already_admin->num_rows() != 0)
	{
		return TRUE;
	}

}

function _validate_admin($username)
{
	$this->load->module('acp_sct');
	$this->load->module('store_accounts');

	$col1 = 'username';
	$value1 = $username;
	$col2 = 'email';
	$value2 = $username;

	$query = $this->store_accounts->get_with_double_condition($col1, $value1, $col2, $value2);

	$num_rows = $query->num_rows();

	if ($num_rows < 1)
	{
		redirect('site_security/not_allowed');
	}

	foreach ($query->result() as $row)
	{
		$veri_id = $row->id;
		$offer_token = $row->veri_token;
	}

	if ($offer_token == '')
	{
		redirect('site_security/not_allowed');
	}

	$comp_token = $this->acp_sct->_get_comp_token($veri_id);

	if ($comp_token === $offer_token)
	{
		return TRUE;
	}
	else
	{
		$this->form_validation->set_message('password_check', 'Attempting to break into this website will result in immediate IP ban.');
		return FALSE;
	}
}

function _recaptcha_validation($response)
{
	$this->load->module('site_settings');

	// Recaptcha validation goes here. I had a function built, but I don't know what the fuck happened to it.
	$secret = $this->site_settings->_get_recaptcha_key();
	$ip = $_SERVER['REMOTE_ADDR'];
	$captcha = $response;
	$rsp = $this->_fOpenRequest("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$captcha&remoteip=$ip");
	$arr = json_decode($rsp, TRUE);
	if($arr['success']) {
		return TRUE;
	} else {
		return FALSE;
	}
}

function _fOpenRequest($url) {
   $file = fopen($url, 'r');
   $data = stream_get_contents($file);
   fclose($file);
   return $data;
}

function _check_is_logged_in()
{
	//check to see if user is logged in
	$user_id = $this->_get_user_id();
	if (!is_numeric($user_id))
	{
		redirect('your_account/login');
	}

}

function _get_user_id()
{
	//attempt to get user_id

	//check for session variable
	$user_id = $this->session->userdata('user_id');

	//if no set session, check for cookie
	if (!is_numeric($user_id))
	{
		$this->load->module('site_cookies');
		$user_id = $this->site_cookies->_attempt_get_user_id();
	}

	return $user_id;
}

function generate_random_string($length)
{
	$characters = '23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
	$randomString = '';
	for ($i = 0; $i < $length; $i++)
	{
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}

function _hash_string($str)
{
	$hashed_string = password_hash($str, PASSWORD_BCRYPT, array(
				'cost' => 11
	));
	return $hashed_string;
}

function _verify_hash($plain_text_str, $hashed_string)
{
	$result = password_verify($plain_text_str, $hashed_string);
	return $result;
}

function _encrypt_string($str)
{
	$this->load->library('encryption');
	$encrypted_string = $this->encryption->encrypt($str);
	return $encrypted_string;
}

function _decrypt_string($str)
{
	$this->load->library('encryption');
	$decrypted_string = $this->encryption->decrypt($str);
	return $decrypted_string;
}

function not_allowed()
{
	$data['headline'] = "You are not authorized to view this page.";
	$data['view_file'] = "not_allowed";
	$this->load->module('templates');
	$this->templates->txrr_template($data);
}

function create_hash($pass, $rounds = 9)
{

	$salt = 'Fuck123';

	$saltChars = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
	for ($i = 0;$i < 22;$i++) {
		$salt .= $saltChars[array_rand($saltChars)];
	}
	return crypt($pass, sprintf('$2y$%02d$', $rounds) . $salt);
}

function _admin_check()
{
	$is_admin = $this->session->userdata('is_admin'); // Pulling $is_admin session to check if is admin

	if ($is_admin == 1)
	{
		return TRUE;
	}
	else
	{
		redirect('site_security/not_allowed');
	}
}

}
